import logging
import datetime
from flask import Flask
from flask_restplus import Resource, Api
import pandas_datareader as pdr

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

@api.route('/<string:ticker>')

class Citi_Stock(Resource):
    def get(self, ticker):

        start_date = datetime.datetime.now() - datetime.timedelta(1)        
        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]

        
        print('*'*40)
        print(df)
        print('\n')
        print('*'*40)

        df['Date'] = df.index

        return {'ticker': ticker,
                'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                'close': df.iloc[-1]['Close']}

if __name__ == '__main__':
    app.run(debug=True)
